.PHONY: build

build:
	@read -p "Enter Rust Version: " RUST_VERSION; \
	docker build --label "RUST_VERSION=$$RUST_VERSION" --build-arg RUST_VERSION=$$RUST_VERSION -t gradity/rust-trunk:$$RUST_VERSION -t gradity/rust-trunk:latest .
	docker push gradity/rust-trunk:$$RUST_VERSION
